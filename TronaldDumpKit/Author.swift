//
//  Author.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


public struct QuoteAuthor: Decodable {
    
    // MARK: - Properties
    
    public var id   : String
    public var name : String
    public var bio  : String?
    
    
    // MARK: - Coding keys
    
    enum CodingKeys: String, CodingKey {
        case id = "author_id"
        case name
        case bio
    }
}
