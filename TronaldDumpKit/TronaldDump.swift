//
//  TronaldDump.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


public class TronaldDump {
    
    // MARK: - Properties
    
    private static let sessionManager = SessionManager()
    
    
    // MARK: - Queries
    
    public static func allTags(completion: @escaping AllTagsResultClosure) {
        sessionManager.standardQuery(to: .tags) { (sessionResult) in
            switch sessionResult {
            case .succeess(let data):
                do {
                    let tags = try Parser.parseTags(fromData: data)
                    completion(.success(tags))
                }
                catch {
                    completion(.error(.unexpectedDataFormat))
                }
                
            case .error(let requestError):
                completion(.error(requestError))
            }
        }
    }
    
    
    public static func quotes(for tag: Tag, completion: @escaping QuotesResultClosure) {
        sessionManager.standardQuery(to: .quotes(tag)) { (sessionResult) in
            switch sessionResult {
            case .succeess(let data):
                do {
                    let quotes = try Parser.parseQuotes(fromData: data)
                    completion(.success(quotes))
                }
                catch {
                    completion(.error(.unexpectedDataFormat))
                }
            case .error(let requestError):
                completion(.error(requestError))
            }
        }
    }
    
}
