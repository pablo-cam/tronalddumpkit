//
//  Parser.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


class Parser {
    
    static func parseTags(fromData data: Data) throws -> [Tag] {
        let decoder = JSONDecoder()
        let decodedTags = try decoder.decode(TagsResponse.self, from: data)
        
        return decodedTags.tags
    }
    
    
    static func parseQuotes(fromData data: Data) throws -> [Quote] {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(QuotesResponse.dateFormatter)
        let decodedQuotes = try decoder.decode(QuotesResponse.self, from: data)
        
        return decodedQuotes.quotes
    }
}
