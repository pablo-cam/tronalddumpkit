//
//  SessionManager.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


class SessionManager {
    
    // MARK: - Properties
    
    private let session = URLSession(configuration: .default)
    
    
    // MARK: - Queries
    
    func standardQuery(to endpoint: Endpoint,
                       queryItems: [URLQueryItem]? = nil,
                       completion: @escaping (SessionResult) -> Void) {
        // Create URL
        let components = URLComponents(for: endpoint, with: queryItems)
        guard let url = components.url else { fatalError("This would be a programmer error") }
        
        // Create request
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        request.addValue("application/hal+json", forHTTPHeaderField: "accept")
        
        // Create task
        let task = session.dataTask(with: request) { (data, response, error) in
            
            // Check for possible errors
            if let requestError = self.posibleRequestError(from: response,
                                                           data: data,
                                                           and: error) {
                self.onMainThread(completion(.error(requestError)))
                return
            }
            
            // Make sure data was received
            guard let data = data else {
                self.onMainThread(completion(.error(.noDataReceived)))
                return
            }
            
            self.onMainThread(completion(.succeess(data)))
        }
        task.resume()
    }
    
    
    // MARK: - Helpers
    
    private func posibleRequestError(from response: URLResponse?,
                                     data: Data?,
                                     and error: Error?) -> RequestError? {
        // Check there was no connection error
        guard error == nil else {
            return RequestError.connectionFailed
        }
        
        // Check the response is valid
        guard let response = response as? HTTPURLResponse else {
            return RequestError.unexpectedServerResponse
        }
        
        // Check the response is OK and data was received
        guard response.statusCode == ResponseStatusCode.success.rawValue else {
            return RequestError.responseError(ResponseStatusCode(rawValue: response.statusCode))
        }
        
        return nil
    }
    
    
    private func onMainThread(_ closure: @escaping @autoclosure () -> Void) {
        DispatchQueue.main.async {
            closure()
        }
    }
}
