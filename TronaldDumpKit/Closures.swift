//
//  Closures.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


public typealias AllTagsResultClosure = (AllTagsResult) -> Void
public typealias QuotesResultClosure  = (QuotesResult)  -> Void

