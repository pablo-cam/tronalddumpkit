//
//  URLComponents+TrolandDump.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


extension URLComponents {
    
    init(for endpoint: Endpoint,
         with queryItems: [URLQueryItem]? = nil) {
        self.init()
        
        self.scheme = APISchema.scheme
        self.host = APISchema.location
        self.path = endpoint.string()
        self.queryItems = queryItems
    }
}
