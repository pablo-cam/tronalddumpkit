//
//  RequestError.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//


import Foundation


// MARK: - Request Error

public enum RequestError: Error
{
    case connectionFailed
    case responseError(ResponseStatusCode?)
    case unexpectedServerResponse
    case unexpectedDataFormat
    case noDataReceived
}


// MARK: - Equatable conformance

extension RequestError: Equatable
{
    public static func ==(lhs: RequestError, rhs: RequestError) -> Bool
    {
        switch (lhs, rhs)
        {
        case (.connectionFailed, .connectionFailed),
             (.unexpectedServerResponse, .unexpectedServerResponse),
             (.unexpectedDataFormat, .unexpectedDataFormat),
             (.noDataReceived, .noDataReceived):
            return true
            
        case (let .responseError(codeA), let .responseError(codeB)):
            return codeA == codeB
            
        default:
            return false
        }
    }
}
