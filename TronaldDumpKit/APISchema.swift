//
//  APISchema.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


struct APISchema {
    static let scheme   = "https"
    static let location = "api.tronalddump.io"
}
