//
//  QuotesResponse.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


struct QuotesResponse {
    
    // MARK: - Properties
    
    var quotes : [Quote]
    
    static var dateFormatter : DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }
    
    
    // MARK: - Coding keys
    
    enum CodingKeys: String, CodingKey {
        case embeddedContainer = "_embedded"
    }
    
    
    // MARK: - Additional coding keys
    
    enum EmbeddedKeys: String, CodingKey {
        case tags
    }
}


// MARK: - Custom decoding

extension QuotesResponse: Decodable {
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let embeddedContainer = try values.nestedContainer(keyedBy: EmbeddedKeys.self, forKey: .embeddedContainer)
        
        quotes = try embeddedContainer.decode([Quote].self, forKey: .tags)
    }
}
