//
//  TagsResponse.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


public typealias Tag = String

struct TagsResponse: Decodable {
    
    // MARK: - Properties
    
    var tags : [Tag]
    
    
    // MARK: - Coding keys
    
    enum CodingKeys: String, CodingKey {
        case tags = "_embedded"
    }
}
