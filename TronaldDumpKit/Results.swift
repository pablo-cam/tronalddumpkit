//
//  Results.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//


import Foundation


public enum AllTagsResult {
    case success([Tag])
    case error(RequestError)
}


public enum QuotesResult {
    case success([Quote])
    case error(RequestError)
}


internal enum SessionResult {
    case succeess(Data)
    case error(RequestError)
}
