//
//  Endpoint.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


enum Endpoint {
    case tags
    case quotes(Tag)
    
    func string() -> String {
        switch self {
        case .tags:
            return "/tag"
            
        case .quotes(let tag):
            return "/tag/" + tag
        }
    }
    
}
