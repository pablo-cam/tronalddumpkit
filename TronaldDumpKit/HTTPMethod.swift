//
//  HTTPMethod.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


enum HTTPMethod : String
{
    case get  = "GET"
    case post = "POST"
    case put  = "PUT"
}
