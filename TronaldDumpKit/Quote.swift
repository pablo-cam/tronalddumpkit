//
//  Quote.swift
//  TronaldDumpKit
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation


public struct Quote {
    
    // MARK: - Properties
    
    public var id : String
    
    public var text   : String
    public var author : QuoteAuthor
    public var tags   : [Tag]
    
    public var creationDate   : Date
    public var updateDate     : Date
    
    
    // MARK: - Coding keys
    
    enum CodingKeys: String, CodingKey {
        case id = "quote_id"
        case text = "value"
        case tags
        case appearanceDate = "appeared_at"
        case creationDate = "created_at"
        case updateDate = "updated_at"
        
        case embeddedContainer = "_embedded"
    }
    
    
    // MARK: - Additional coding keys
    
    enum EmbededKeys: String, CodingKey {
        case author
    }
    
}


// MARK: - Custom decoding

extension Quote: Decodable {
    
    public init(from decoder: Decoder) throws {
        // Get containers
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let embeddedContainer = try values.nestedContainer(keyedBy: EmbededKeys.self, forKey: .embeddedContainer)
        
        // Decode values
        id = try values.decode(String.self, forKey: .id)
        
        text   = try values.decode(String.self, forKey: .text)
        author = try embeddedContainer.decode([QuoteAuthor].self, forKey: .author)[0]
        tags   = try values.decode([Tag].self, forKey: .tags)
        
        creationDate   = try values.decode(Date.self, forKey: .creationDate)
        updateDate     = try values.decode(Date.self, forKey: .updateDate)
    }
}
