//
//  TronaldDumpTests.swift
//  TronaldDumpKitTests
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import XCTest
@testable import TronaldDumpKit


let ExpectationTimeout = 10.0


class TronaldDumpTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    // MARK: - Test tags
    
    func testGettingAllTags() {
        let allTagsExpectation = expectation(description: "All tags must be received")
        
        TronaldDump.allTags { (result) in
            switch result {
            case .success(_):
                allTagsExpectation.fulfill()
            case .error(_):
                XCTFail()
            }
        }
        
        wait(for: [allTagsExpectation], timeout: ExpectationTimeout)
    }
    
    
    func testGettingAllTagsCompletionHandlerIsOnMainThread() {
        let mainThreadExpectation = expectation(description: "Completion Handler Is On Main Thread")
        
        TronaldDump.allTags { (result) in
            if Thread.isMainThread {
                mainThreadExpectation.fulfill()
            }
            else {
                XCTFail()
            }
        }
        
        wait(for: [mainThreadExpectation], timeout: ExpectationTimeout)
    }
    
    
    // MARK: - Testing quotes
    
    func testGettingQuotesForAValidTag() {
        let quotesReceivedExpectation = expectation(description: "Receive a list of quotes")
        let tag = "Mexico"
        
        TronaldDump.quotes(for: tag) { (quotesResult) in
            switch quotesResult {
            case .success(_):
                quotesReceivedExpectation.fulfill()
            case .error(_):
                XCTFail()
            }
        }
        
        wait(for: [quotesReceivedExpectation], timeout: ExpectationTimeout)
    }
    
    
    func testGettingQuotesCompletionHandlerIsOnMainThread() {
        let mainThreadExpectation = expectation(description: "Completion Handler Is On Main Thread")
        let tag = "Mexico"
        
        TronaldDump.quotes(for: tag) { (quotesResult) in
            if Thread.isMainThread {
                mainThreadExpectation.fulfill()
            }
            else {
                XCTFail()
            }
        }
        
        wait(for: [mainThreadExpectation], timeout: ExpectationTimeout)
    }
    
}
